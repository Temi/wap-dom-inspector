// WAP - DOM inspector
// Hana Brychtova, xbrych02

//funkce na editovani
function addClass(element,value,informationNode)
{
  //nemuzu pridat zadnou z mych trid v css:
  //.wap_active .wap_ignore .wap_hidden .wap_editor
  if (value == "wap_ignore" || value == "wap_hidden" || value == "wap_editor" || value == "wap_active")
  {
    alert("Error: Tato trida nelze pridat, je vyuzivana aplikaci DOM inspector.");
  }
  else
  {
    if (!element.classList.contains(value))
    {
      element.classList.add(value);
      informationNode.innerHTML = "Class = "+element.className.split(' ');
    }
    else
    {
      alert("Error: Snazite se pridat tridu, ve ktere uz element je.");
    }
  }
}

function removeClass(element,value,informationNode)
{
   if (element.classList.contains(value))
    {
      element.classList.remove(value);
      informationNode.innerHTML = "Class = "+element.className.split(' ');
    }
    else
    {
      alert("Error: Snazite se pridat tridu, ve ktere element neni.");
    }  
}

function modifyId(element,value,informationNode)
{
  if (!document.getElementById(value) || element.id==value)
  {
    element.id = value;
    informationNode.innerHTML = "Id = "+value;
  }
  else
  {
    alert("Error: Toto ID se jiz v dokumentu vyskytuje.");
  }

}

// vraci prave vybrany DOM element nebo null
function getSelectedElement(event)
{
  var element;
  if (event.target)
  {
    element = event.target;
  }
  else if (event.srcElement)
  {
    element = event.srcElement;
  }

  //kontrola jestli je to soucast DOM, jinak null
  // prvne prohledam nody
  for (var i=0;i<array.length;i++)
      {
	//prvek je v poli
	if (element === array[i].inspectorNode || element === array[i].pageNode)
	{
	   return element;
	}
	
      }
    // pak prohledam ostatni - kvuli textovym elementum
     for (var i=0;i<array.length;i++)
      {
    	//nebo jsem neprazdny textovy node a muj rodic je v poli
	if (element.parentNode.parentNode === array[i].inspectorNode || element.parentNode.parentNode === array[i].pageNode)
	{
	  //neprazdny a neni soucast editoru
	  if (/\S/.test(element) && !element.classList.contains("wap_editor"))
	  {
	    return element.parentNode.parentNode;
	  }
	}
      }
  
  return null;
}


function handleClick(element)
{
    // jestli je vybran, tak odeber tridu active
    if (element.classList.contains("wap_active"))
    {
      // odeber z vybraneho i z toho, na nej napojeny
      element.classList.remove('wap_active');
    
    for (var i=0;i<array.length;i++)
    {
      if (element === array[i].inspectorNode)
      {
	  array[i].pageNode.classList.remove('wap_active');
	  array[i].editNode.classList.add('wap_hidden'); // a schovej editaci
      }
      else if (element === array[i].pageNode)
      {
	array[i].inspectorNode.classList.remove('wap_active');
	array[i].editNode.classList.add('wap_hidden'); // a schovej editaci
      }
    }
    }
    //jestli neni vybran, tak pridej tridu active
    else
    {
      // pridej na vybrany i na nej napojeny
      element.classList.add('wap_active');
    
      for (var i=0;i<array.length;i++)
      {
	if (element === array[i].inspectorNode)
	{
	    array[i].pageNode.classList.add('wap_active');
	    array[i].editNode.classList.remove('wap_hidden'); // zobraz editaci
	}
	else if (element === array[i].pageNode)
	{
	  array[i].inspectorNode.classList.add('wap_active');
	  array[i].editNode.classList.remove('wap_hidden'); // zobraz editaci
	}
      }
    }
  

}

//vypis DOM hiearchicky jako zanorene seznamy
function writeDOM(pageElement,parentList)
{     
   // pridane elementy a komentare ignoruj
   if (pageElement.className == "wap_ignore" || pageElement.nodeType == Node.COMMENT_NODE )
    {
      return;
    }
    
    //vytvor prvek
	var node = document.createElement('li');
	    node.classList.add('wap_ignore');
	    node.innerHTML = pageElement.nodeName;
      

    //pom promenna kvuli whitespace
      var whitespace = false;
      
    // pokud jsem element typ, pridam tridu na editaci class a id
     if (pageElement.nodeType == Node.ELEMENT_NODE)
     {
       var editor = document.createElement('ul');
	  editor.classList.add('wap_ignore');
	  editor.classList.add('wap_editor');
	  editor.classList.add('wap_hidden'); //a skryju
	  
      var editId = document.createElement('li');
	  editId.classList.add('wap_ignore');
	  editId.classList.add('wap_editor');
	  
      var infoId = document.createElement('span');
	  infoId.classList.add('wap_editor');
	  infoId.innerHTML = "Id = "+pageElement.id;
	  
      //formular ke zmene ID
      var formId = document.createElement('form');
	  formId.classList.add('wap_editor');
      var changeId = document.createElement('input');
	  changeId.setAttribute('type','text');
	  changeId.classList.add('wap_editor');
      var changeIdSubmit = document.createElement('input');
	  changeIdSubmit.setAttribute('type','button');
	  changeIdSubmit.setAttribute('value','Change');
	  changeIdSubmit.classList.add('wap_editor');

      var editClasses = document.createElement('li');
	  editClasses.classList.add('wap_ignore');
	  editClasses.classList.add('wap_editor');
	  
      var infoClasses = document.createElement('span');
	  infoClasses.classList.add('wap_editor');
	  infoClasses.innerHTML = "Class = "+pageElement.className.split(' ');
	  
      //formular ke zmene trid
      var formClasses = document.createElement('form');
	  formClasses.classList.add('wap_editor');
      var changeClasses = document.createElement('input');
	  changeClasses.setAttribute('type','text');
	  changeClasses.classList.add('wap_editor');
      var addClassSubmit = document.createElement('input');
	  addClassSubmit.setAttribute('type','button');
	  addClassSubmit.setAttribute('value','Add'); 
	  addClassSubmit.classList.add('wap_editor');
      var removeClassSubmit = document.createElement('input');
	  removeClassSubmit.setAttribute('type','button');
	  removeClassSubmit.setAttribute('value','Remove'); 
	  removeClassSubmit.classList.add('wap_editor');
	  
      // pridame odchytavani kliknuti
      changeIdSubmit.addEventListener('click', function() {
	    
	    modifyId(pageElement,changeId.value,infoId);
	  });
      addClassSubmit.addEventListener('click', function() {
	    
	    addClass(pageElement,changeClasses.value,infoClasses);
	  });
      removeClassSubmit.addEventListener('click', function() {
	    
	    removeClass(pageElement,changeClasses.value,infoClasses);
	  });

	
      editId.appendChild(infoId);
      formId.appendChild(changeId);
      formId.appendChild(changeIdSubmit);
      editId.appendChild(formId);
      editor.appendChild(editId);
      
      editClasses.appendChild(infoClasses);
      formClasses.appendChild(changeClasses);
      formClasses.appendChild(addClassSubmit);
      formClasses.appendChild(removeClassSubmit);
      editClasses.appendChild(formClasses);
      editor.appendChild(editClasses);
      
      node.appendChild(editor); 
     }
    
   // pokud ma element nejake deti, vytvor novy podseznam a zavolej writeDOM nad vsemi netextovymi - textove vypis
     if (pageElement.childNodes.length > 0)
     {
       
       var newList = document.createElement('ul');
	  newList.classList.add('wap_ignore');
	  node.appendChild(newList); 

     for (var i=0, max=pageElement.childNodes.length; i < max; i++)
     {
	  writeDOM(pageElement.childNodes[i],newList); 
      }
     }
     else
     {
       if (pageElement.nodeType == Node.TEXT_NODE)
	{
	  // jde jen o whitespace?
	  if (/^\s+$/.test(pageElement.data))
	  {
	    whitespace = true;
	  }
	  node.innerHTML = pageElement.data;
	  
	}
	// komentar atd
	else if (pageElement.nodeType != Node.ELEMENT_NODE)
	{
	  return;
	}
     }
     
     // pripnu uzel do seznamu a pole pokud to neni whitespace
     if (!whitespace)
     {
      parentList.appendChild(node); 
     }
      
     if (pageElement.nodeType == Node.ELEMENT_NODE)
     {
      // pridame elementy do pole
      array.push({
        inspectorNode: node,
        pageNode: pageElement,
	editNode: editor
      }); 
     }
 
}

// pridej oblast, kde bude vypsany strom
function attachInspector()
{
var s = document.createElement('link');
	s.setAttribute('rel', 'stylesheet');
	s.setAttribute('type', 'text/css');
	s.setAttribute('href', 'wap_style.css');
	s.classList.add('wap_ignore');
    
document.head.appendChild(s);

var inspector = document.createElement('div');
    inspector.setAttribute('id','wap_inspector');
    inspector.classList.add('wap_ignore');
    
var list = document.createElement('ul');
    inspector.appendChild(list);
      
document.body.appendChild(inspector);

return list;
}

function execute()
{
  // pridame panel s inspectorem
  parentList = attachInspector();
  // vypis DOM jako hiearchicky vnorene seznamy; rekurze
  writeDOM(document, parentList);
  
  // pridame odchytavani kliknuti
  document.addEventListener('click', function(event) {
    var selected = getSelectedElement(event);
    if (selected != null)
      handleClick(selected);
    });

}

// pom. pole
var array = [];

window.onload = execute;




